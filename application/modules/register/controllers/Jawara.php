<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jawara extends CI_Controller{
     function __construct(){
          parent::__construct();

          $this->load->model('Jawara_model', 'modelJawara');
          $this->load->helper(array('form', 'url'));
          $this->load->library('form_validation');
     }

     function uploadKTP()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_ktp']['name'];
			$tmpname = $_FILES['file_ktp']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'KTP'.date('Ymdhis').".". $ext;
			$config['upload_path'] = './assets/berkas/ktp/';
			$config['upload_url'] =  base_url() . 'assets/berkas/ktp/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/ktp/" . $newname);
			return $newname;
		}
	}

     function uploadKK()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_kk']['name'];
			$tmpname = $_FILES['file_kk']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'KK'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/kk/';
			$config['upload_url'] =  base_url() . 'assets/berkas/kk/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/kk/" . $newname);
			return $newname;
		}
	}

     function uploadStore()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_store']['name'];
			$tmpname = $_FILES['file_store']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'STORE'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/store/';
			$config['upload_url'] =  base_url() . 'assets/berkas/store/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/store/" . $newname);
			return $newname;
		}
	}

     function index(){
          // $x['provinsi'] = $this->modelJawara->get_provinsi();

          // $this->load->view("include/frontend/head");
          // $this->load->view('index', $x);
          // $this->load->view("include/frontend/footer");
          // $this->load->view("include/alert");

          redirect();
     }

     function lanjutan(){
          // $x['provinsi'] = $this->modelJawara->get_provinsi();

          // $this->load->view("include/frontend/head");
          // $this->load->view('jawara-new', $x);
          // $this->load->view("include/frontend/footer");
          // $this->load->view("include/alert");

          redirect();
     }

     // request data kecamatan berdasarkan id PROVINSI yang dipilih
     function get_kabupaten()
     {
          if ($this->input->post('provinsi_id')) {
               echo $this->modelJawara->get_kabupaten($this->input->post('provinsi_id'));
          }
     }

     //request data kecamatan berdasarkan id kabupaten yang dipilih
     function get_kecamatan()
     {
          if ($this->input->post('kabupaten_id')) {
               echo $this->modelJawara->get_kecamatan($this->input->post('kabupaten_id'));
          }
     }

     public function leds(){

          $this->form_validation->set_rules('email', 'Email', 'required');
          $this->form_validation->set_rules('name', 'Name', 'required');
          $this->form_validation->set_rules('phone', 'Phone', 'required');
          $this->form_validation->set_rules('nik_ktp', 'Nik KTP', 'required');
          $this->form_validation->set_rules('nik_kk', 'Nik KK', 'required');
          $this->form_validation->set_rules('pinjaman', 'Status Pinjaman', 'required');
          $this->form_validation->set_rules('alamat_rumah', 'Alamat Rumah', 'required');
          $this->form_validation->set_rules('rt_rumah', 'RT Rumah', 'required');
          $this->form_validation->set_rules('rw_rumah', 'RW Rumah', 'required');
          $this->form_validation->set_rules('nomor_rumah', 'Nomor Rumah', 'required');
          $this->form_validation->set_rules('alamat_usaha', 'Alamat Usaha', 'required');
          $this->form_validation->set_rules('rt_usaha', 'RT Usaha', 'required');
          $this->form_validation->set_rules('rw_usaha', 'RW Usaha', 'required');
          $this->form_validation->set_rules('nomor_usaha', 'Nomor Usaha', 'required');
          $this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
          $this->form_validation->set_rules('kab_kota', 'Kabupaten/Kota', 'required');
          $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
          $this->form_validation->set_rules('kodepos', 'Kode Pos', 'required');

          $this->form_validation->set_rules('term_condition', 'Term Condition', 'required');

          if ($this->form_validation->run() == FALSE)
          {
               $this->session->set_flashdata('success','Hayoloh Anda Siapa ? :P');
               redirect();
          }
          else
          {
               $ktp = '';
               $kk = '';
               $store = '';

               if (!empty($_FILES['file_ktp']['name'])) {
                    $newname = $this->uploadKTP();
                    $data['file_ktp'] = $newname;
                    $ktp = $newname;
               }

               if (!empty($_FILES['file_kk']['name'])) {
                    $newname = $this->uploadKK();
                    $data['file_kK'] = $newname;
                    $kk = $newname;
               }

               if (!empty($_FILES['file_store']['name'])) {
                    $newname = $this->uploadStore();
                    $data['file_store'] = $newname;
                    $store = $newname;
               }
               
               $x['email'] = $this->input->post('email');
               $x['name'] = $this->input->post('name');
               $x['phone'] = $this->input->post('phone');
               $x['nik_ktp'] = $this->input->post('nik_ktp');
               $x['nik_kk'] = $this->input->post('nik_kk');
               $x['pinjaman'] = $this->input->post('pinjaman');
               $x['alamat_rumah'] = $this->input->post('alamat_rumah');
               $x['rt_rumah'] = $this->input->post('rt_rumah');
               $x['rw_rumah'] = $this->input->post('rw_rumah');
               $x['nomor_rumah'] = $this->input->post('nomor_rumah');
               
               $x['file_ktp'] = base_url('assets/berkas/ktp/').$ktp;
               $x['file_kk'] = base_url('assets/berkas/kk/').$kk;

               $x['alamat_usaha'] = $this->input->post('alamat_usaha');
               $x['rt_usaha'] = $this->input->post('rt_usaha');
               $x['rw_usaha'] = $this->input->post('rw_usaha');
               $x['nomor_usaha'] = $this->input->post('nomor_usaha');
               $x['provinsi'] = $this->modelJawara->cekProvinsi($this->input->post('provinsi'));
               $x['kab_kota'] = $this->modelJawara->cekKabupaten($this->input->post('kab_kota'));
               $x['kecamatan'] = $this->input->post('kecamatan');
               $x['kodepos'] = $this->input->post('kodepos');
               $x['file_store'] = base_url('assets/berkas/store/').$store;

               if($x['kab_kota'] == 'KOTA TANGERANG' || 
               $x['kab_kota'] == 'KABUPATEN TANGERANG' || 
               $x['kab_kota'] == 'KOTA TANGERANG SELATAN'
               ){
                    $x['area_jawara'] = 'JABODETABEK - BANTEN';

               }
               
               else if($x['kab_kota'] == 'KOTA JAKARTA UTARA' ||
               $x['kab_kota'] == 'KOTA JAKARTA SELATAN' ||
               $x['kab_kota'] == 'KOTA JAKARTA TIMUR' ||
               $x['kab_kota'] == 'KOTA JAKARTA BARAT' ||
               $x['kab_kota'] == 'KOTA JAKARTA PUSAT'
               ){
                    $x['area_jawara'] = 'JABODETABEK - DKI JAKARTA';
               }

               else if($x['kab_kota'] == 'KOTA BOGOR' ||
               $x['kab_kota'] == 'KABUPATEN BOGOR' ||
               $x['kab_kota'] == 'KOTA BEKASI' ||
               $x['kab_kota'] == 'KABUPATEN BEKASI' ||
               $x['kab_kota'] == 'KOTA DEPOK' ||
               $x['kab_kota'] == 'KABUPATEN KARAWANG'
               ){
                    $x['area_jawara'] = 'JABODETABEK - JAWA BARAT';
               }

               else if($x['kab_kota'] == 'KOTA SURABAYA' ||
               $x['kab_kota'] == 'KABUPATEN MOJOKERTO' ||
               $x['kab_kota'] == 'KOTA MOJOKERTO' ||
               $x['kab_kota'] == 'KABUPATEN GRESIK' ||
               $x['kab_kota'] == 'KABUPATEN SIDOARJO'
               ){
                    $x['area_jawara'] = 'SURABAYA RAYA';
               }

               else if($x['kab_kota'] == 'KOTA BANDUNG' ||
               $x['kab_kota'] == 'KABUPATEN BANDUNG' ||
               $x['kab_kota'] == 'KABUPATEN BANDUNG BARAT' ||
               $x['kab_kota'] == 'KOTA CIMAHI' ||
               $x['kab_kota'] == 'KABUPATEN SUMEDANG'
               ){
                    $x['area_jawara'] = 'BANDUNG RAYA';
               }

               else if($x['provinsi'] == 'KALIMANTAN UTARA' ||
               $x['provinsi'] == 'KALIMANTAN BARAT' ||
               $x['provinsi'] == 'KALIMANTAN SELATAN' ||
               $x['provinsi'] == 'KALIMANTAN TIMUR' ||
               $x['provinsi'] == 'KALIMANTAN TENGAH'
               ){
                    $x['area_jawara'] = 'KALIMANTAN DAN SAMARINDA';
               }else{
                    $x['area_jawara'] = 'LAINNYA';
               }

               $kodeReferral = $this->input->post('kode_referral');
               $kodeReferral2 = $this->input->post('kode_referral2');
               if($kodeReferral == 'LAINNYA'){
                    if($kodeReferral2 == NULL){
                         $x['kode_referral'] = $this->input->post('kode_referral');
                    }else{
                         $x['kode_referral'] = strtoupper($this->input->post('kode_referral2'));
                    }
               }else{
                    $x['kode_referral'] = $this->input->post('kode_referral');
               }

               // Cek Tanggal Lahir
               $a['tanggal_lahir'] = substr($x['nik_ktp'], 6, 2);
               if (intval($a['tanggal_lahir']) > 40) { 
                    $a['tanggal_lahir'] = intval($a['tanggal_lahir']) - 40; 
                    $gender = 'WANITA'; 
               } else { 
                    $a['tanggal_lahir'] = intval($a['tanggal_lahir']); 
                    $gender = 'PRIA'; 
               }

               // Cek Bulan Lahir
               $a['bulan_lahir'] = substr($x['nik_ktp'], 8, 2); 

               // Cek Tahun Lahir
               // $a['tahun_lahir'] = substr($x['nik_ktp'], 10, 2);
               // if (intval($a['tahun_lahir']) <= 40) { 
               //      $a['tahun_lahir'] = '20'.intval($a['tahun_lahir']);
               // } else { 
               //      $a['tahun_lahir'] = '19'.intval($a['tahun_lahir']);
               // }

               $tahun_lahir = substr($x['nik_ktp'], 10, 2);
               $cek_tahun1 = substr($tahun_lahir, 0, 1);
               $cek_tahun2 = substr($tahun_lahir, 1, 2);

               if($cek_tahun1 == 0){
                    if (intval($cek_tahun2) <= 40) { 
                         $a['tahun_lahir'] = '20'.$tahun_lahir;
                    } else { 
                         $a['tahun_lahir'] = '19'.$tahun_lahir;
                    }
               }else{
                    if (intval($tahun_lahir) <= 40) { 
                         $a['tahun_lahir'] = '20'.$tahun_lahir;
                    } else { 
                         $a['tahun_lahir'] = '19'.$tahun_lahir;
                    }
               }

               $x['tanggal_lahir'] = $a['tahun_lahir'].'-'.$a['bulan_lahir'].'-'.$a['tanggal_lahir'];
               $x['gender'] = $gender;

               $x['created_at'] = date('Y-m-d H:i:s');
               $x['term_condition'] = $this->input->post('term_condition');

               $result = $this->modelJawara->leds($x);

               redirect();
          }

     }

     public function awalan(){

          $this->form_validation->set_rules('name_awal', 'Name', 'required');
          $this->form_validation->set_rules('phone_awal', 'Phone', 'required');

          if ($this->form_validation->run() == FALSE)
          {
               $this->session->set_flashdata('success','Hayoloh Anda Siapa ? :P');
               redirect();
          }
          else
          {
               $x['name'] = $this->input->post('name_awal');
               $x['phone'] = $this->input->post('phone_awal');
               $x['created_at'] = date('Y-m-d H:i:s');

               $this->modelJawara->awalan($x);

               redirect('index.php/jawara/lanjutan?name='.$x['name'].'&phone='.$x['phone'].'#daftar');
          }

     }


}