	<section class="page-section m-5" id="daftar">
		<div class="container">
			<div class="text-center">
				<h4 class="monst">JApang WArung RAkyat</h4>
				<p class="section-subheading text-muted">Selamat ! Selangkah lebih dekat lagi untuk memiliki bisnis dengan dimodalin oleh Jaring Pangan Indonesia.</p>
            </div>
			<div class="row text-center">
				<form action="<?= base_url('index.php/register/jawara/leds') ?>" method="POST" name="form-wizard" class="form-control-with-bg" enctype="multipart/form-data">
					<!-- begin wizard -->
					<div id="wizard">
						<!-- begin wizard-step -->
						<ul>
							<li>
								<a href="#step-1">
									<span class="number">1</span> 
									<span class="info">
										Data Diri
										<small></small>
									</span>
								</a>
							</li>
							<li>
								<a href="#step-2">
									<span class="number">2</span> 
									<span class="info">
										Upload Dokumen
										<small></small>
									</span>
								</a>
							</li>
							<li>
								<a href="#step-3">
									<span class="number">3</span>
									<span class="info">
										Lokasi Usaha
										<small></small>
									</span>
								</a>
							</li>
							<li>
								<a href="#step-4">
									<span class="number">4</span> 
									<span class="info">
										Simpan
										<small></small>
									</span>
								</a>
							</li>
						</ul>
						<!-- end wizard-step -->
						<!-- begin wizard-content -->
						<div>
							<!-- begin step-1 -->
							<div id="step-1">
								<!-- begin fieldset -->
								<fieldset>
									<!-- begin row -->
									<div class="row">
										<!-- begin col-8 -->
										<div class="col-xl-8 offset-xl-2">
											<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Personal info JAWARA</legend>
											
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Nama<span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<input type="text" name="name" pattern="[A-Z a-z]{3,50}" placeholder="Nama Sesuai KTP" data-parsley-group="step-1" data-parsley-required="true" class="form-control" value="<?= $this->input->get('name') ?>"/>
												</div>
											</div>
											<!-- end form-group -->

											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Email <span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<input type="email" name="email" placeholder="Email" data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
												</div>
											</div>
											<!-- end form-group -->

											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Nomor Handphone <span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<input type="tel" pattern="^628[0-9]{8,}$" name="phone" placeholder="628xxxxxxxxx" data-parsley-group="step-1" data-parsley-required="true" data-parsley-type="number" class="form-control" value="<?= $this->input->get('phone') ?>"/>
													<span style="font-size: 12px;">Note: 628xxxxxxxxx (Terdiri dari panjang minimal 10 dan selalu dimulai dengan 628)</span>
												</div>
											</div>
											<!-- end form-group -->

											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">No Induk KTP <span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<input type="text" pattern="^[0-9]{16,}$" name="nik_ktp" placeholder="No Induk KTP" data-parsley-group="step-1" data-parsley-required="true" data-parsley-type="number" class="form-control" />
													<span style="font-size: 12px;">Nomor KTP 16 Digit.</span>
												</div>
											</div>
											<!-- end form-group -->

											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">No Induk KK <span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<input type="text" pattern="^[0-9]{16,}$" name="nik_kk" placeholder="No Induk KK" data-parsley-group="step-1" data-parsley-required="true" data-parsley-type="number" class="form-control" />
													<span style="font-size: 12px;">Nomor KK 16 Digit.</span>
												</div>
											</div>
											<!-- end form-group -->

											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Apakah anda pernah mengajukan pinjaman? <span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<select class="form-control" name="pinjaman" searchable="Search here.." data-parsley-group="step-1" data-parsley-required="true" >
														<option value="" disabled selected>Choose your status</option>
														<option value="Ya : sudah selesai dan mengalami kredit macet">Ya : sudah selesai dan mengalami kredit macet</option>
														<option value="Ya : sudah selesai dan lancar">Ya : sudah selesai dan lancar</option>
														<option value="Ya : sedang berjalan dan mengalami kredit macet">Ya : sedang berjalan dan pernah mengalami kredit macet</option>
														<option value="Ya : sedang berjalan dan tidak pernah macet">Ya : sedang berjalan dan tidak pernah macet</option>
														<option value="Tidak">Tidak</option>
													</select>
												</div>
											</div>
											<!-- end form-group -->

											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Alamat Rumah<span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<textarea name="alamat_rumah" placeholder="Alamat Lengkap Rumah" data-parsley-group="step-1" data-parsley-required="true" class="form-control" rows="4" cols="50"></textarea>
												</div>
											</div>
											<!-- end form-group -->

											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">RT<span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<input type="text" pattern="^[0-9]{3,}$" name="rt_rumah" placeholder="RT" data-parsley-group="step-1" data-parsley-required="true" data-parsley-type="number" class="form-control" />
													<span style="font-size: 12px;">Nomor RT 3 Digit.</span>
												</div>
											</div>
											<!-- end form-group -->

											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">RW<span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<input type="text" pattern="^[0-9]{3,}$" name="rw_rumah" placeholder="RW" data-parsley-group="step-1" data-parsley-required="true" data-parsley-type="number" class="form-control" />
													<span style="font-size: 12px;">Nomor RW 3 Digit.</span>
												</div>
											</div>
											<!-- end form-group -->

											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">No. Rumah<span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
												<input type="text" name="nomor_rumah" placeholder="Nomor Rumah" data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
												</div>
											</div>
											<!-- end form-group -->
										</div>
										<!-- end col-8 -->
									</div>
									<!-- end row -->
								</fieldset>
								<!-- end fieldset -->
							</div>
							<!-- end step-1 -->

							<!-- begin step-2 -->
							<div id="step-2">
								<!-- begin fieldset -->
								<fieldset>
									<!-- begin row -->
									<div class="row">
										<!-- begin col-8 -->
										<div class="col-xl-8 offset-xl-2">
											<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Upload Dokumen Anda</legend>
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Upload Foto KTP Anda <span class="text-danger">Asli *</span></label>
												<div class="col-lg-9 col-xl-6">
													<input type="file" name="file_ktp" accept="image/png, image/jpg, image/jpeg" ata-parsley-group="step-2" data-parsley-required="true" class="form-control"/>
													<span style="font-size: 12px;">Note : Ukuran Foto KTP max. 2000kb.</span>
												</div>
											</div>
											<!-- end form-group -->
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Upload Foto KK Anda <span class="text-danger">Asli *</span></label>
												<div class="col-lg-9 col-xl-6">
													<input type="file" name="file_kk" accept="image/png, image/jpg, image/jpeg" ata-parsley-group="step-2" data-parsley-required="true" class="form-control"/>
													<span style="font-size: 12px;">Note : Ukuran Foto KK max. 2000kb.</span>
												</div>
											</div>
											<!-- end form-group -->
										</div>
										<!-- end col-8 -->
									</div>
									<!-- end row -->
								</fieldset>
								<!-- end fieldset -->
							</div>
							<!-- end step-2 -->

							<!-- begin step-3 -->
							<div id="step-3">
								<!-- begin fieldset -->
								<fieldset>
									<!-- begin row -->
									<div class="row">
										<!-- begin col-8 -->
										<div class="col-xl-8 offset-xl-2">
											<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Lokasi Usaha Anda</legend>
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Alamat Usaha<span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<textarea name="alamat_usaha" placeholder="Alamat Lengkap Rumah" data-parsley-group="step-3" data-parsley-required="true" class="form-control" rows="4" cols="50"></textarea>
												</div>
											</div>
											<!-- end form-group -->
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">RT Usaha<span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
												<input type="text" pattern="^[0-9]{3,}$" name="rt_usaha" placeholder="RT" data-parsley-group="step-3" data-parsley-required="true" data-parsley-type="number" class="form-control" />
												</div>
											</div>
											<!-- end form-group -->
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">RW Usaha<span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
												<input type="text" pattern="^[0-9]{3,}$" name="rw_usaha" placeholder="RW" data-parsley-group="step-3" data-parsley-required="true" data-parsley-type="number" class="form-control" />
												</div>
											</div>
											<!-- end form-group -->
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">No. Rumah Usaha<span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
												<input type="text" name="nomor_usaha" placeholder="Nomor Rumah Usaha" data-parsley-group="step-3" data-parsley-required="true" class="form-control" />
												</div>
											</div>
											<!-- end form-group -->
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Provinsi <span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<select name="provinsi" id="provinsi" class="form-control" data-parsley-group="step-3" data-parsley-required="true">
														<option value="">Pilih Provinsi</option>
														<?php
															foreach ($provinsi as $row) {
																echo '<option value="' . $row->id . '">' . $row->name . '</option>';
															}
														?>
													</select>
												</div>
											</div>
											<!-- end form-group -->
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Kabupaten/Kota <span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<select name="kab_kota" id="kab_kota" class="form-control" data-parsley-group="step-3" data-parsley-required="true">
														<option value="">Pilih Kabupaten</option>
														<?php
															
														?>
													</select>
												</div>
											</div>
											<!-- end form-group -->
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Kecamatan <span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<select name="kecamatan" id="kecamatan" class="form-control" data-parsley-group="step-3" data-parsley-required="true">
														<option value="">Pilih Kecamatan</option>
														<?php
															
														?>
													</select>
												</div>
											</div>
											<!-- end form-group -->
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Kodepos <span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<input type="text" pattern="^[0-9]{5,}$" name="kodepos" placeholder="Kodepos" data-parsley-group="step-3" data-parsley-required="true" data-parsley-type="number" class="form-control" />
													<span style="font-size: 12px;">Kode Pos 5 Digit.</span>
												</div>
											</div>
											<!-- end form-group -->
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Upload tampak depan untuk tempat yang akan digunakan sebagai JAWARA <span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<input type="file" name="file_store" accept="image/png, image/jpg, image/jpeg" ata-parsley-group="step-3" data-parsley-required="true" class="form-control"/>
													<span style="font-size: 12px;">Note : Ukuran Foto max. 2000kb.</span>
												</div>
											</div>
											<!-- end form-group -->
											<!-- begin form-group -->
											<div class="form-group row m-b-10">
												<label class="col-lg-3 text-lg-right col-form-label">Kode Referral <span class="text-danger">*</span></label>
												<div class="col-lg-9 col-xl-6">
													<select name="kode_referral" id="kode_referral" class="form-control" data-parsley-group="step-3" data-parsley-required="true">
														<option value="">Pilih Referral</option>
														<option value="INSTAGRAM">INSTAGRAM</option>
														<option value="YOUTUBE">YOUTUBE</option>
														<option value="FACEBOOK">FACEBOOK</option>
														<option value="GOOGLE">GOOGLE</option>
														<option value="WHATSAPP">WHATSAPP</option>
														<option value="TEMAN/KERABAT">TEMAN/KERABAT</option>
														<option value="KOMUNITAS">KOMUNITAS</option>
														<option value="AMEN">AMEN</option>
														<option value="PHRI">PHRI</option>
														<option value="LAINNYA">LAINNYA</option>
													</select>
												</div>
											</div>
											<!-- end form-group -->
											<div class="initial">
												<div class="form-group row m-b-10">
													<label class="col-lg-3 text-lg-right col-form-label">Kode Referral Lainnya<span class="text-danger">*</span></label>
													<div class="col-lg-9 col-xl-6">
														<input type="text" name="kode_referral2" placeholder="Kode Referral Lainnya" data-parsley-group="step-3" class="form-control"/>
													</div>
												</div>
											</div>
										</div>
										<!-- end col-8 -->
									</div>
									<!-- end row -->
								</fieldset>
								<!-- end fieldset -->
							</div>
							<!-- end step-3 -->

							<!-- begin step-4 -->
							<div id="step-4">
								<div class="jumbotron m-b-0 text-center">
									<h2 class="display-4">Apakah Anda Yakin Dengan Data Anda ?</h2>
									<p class="lead mb-4">Jika sudah Yakin, Klik button Simpan JAWARA di bawah ini.</p>
									<p><div class="form-check">
										<input class="form-check-input" type="checkbox" value="1" name="term_condition" id="term_condition" required>
										<label class="form-check-label" for="term_condition">
											I agree to the 
											<a href="javascript:;" data-toggle="modal" data-target="#termsconditions" style="color: blue">
											terms dan conditions
											</a>
										</label>
									</div>
									</p>
									<p><button type="submit" class="btn btn-primary btn-lg">Simpan JAWARA</button></p>
								</div>
							</div>
							<!-- end step-4 -->
						</div>
						<!-- end wizard-content -->
					</div>
					<!-- end wizard -->
				</form>	
			</div>
		</div>
	</section>

	<section class="page-section" id="jawara">
		<div class="container-fluid mb-5" style="background-color: rgb(77, 125, 182);">
			<div class="row">
				<div class="col-sm-4" >
					<img src="https://jaringpangan.com/jawara/konten/powerranger.png" class="img-fluid" width="300" alt="...">
				</div>
						
				<div class="col-md bg-white my-5 py-5 ps-5">
					<p class="lh-1 pt-3">
						<b>Apa itu JAWARA (JaPang Warung Rakyat)</b>, adalah salah satu program dari JaPang untuk menciptakan pengusaha baru atau mengembangkan usaha yang sudah dimiliki dengan skema permodalan dalam bentuk barang – barang yang diproduksi oleh JaPang.</p>
						<p class="lh-1">
							<b>Sedangkan siapa itu JaPang ?</b> adalah perusahan yang bergerak dibidang agrikultur yang fokusnya adalah ke produk Beras, Ayam, Telur yang bekerja sama dengan petani lokal.
						</p>
						<p class="lh-1">
							<b>Apa keuntungan menjadi JAWARA ?</b>
							Bisa memulai usaha tanpa bingung modal darimana dan bisa mengembangkan usaha tanpa bingung mengumpulkan modal lebih banyak
							Produk yang dijual oleh JAWARA adalah : konsentrasinya di Beras, Ayam, Telur dengan merk JaPang sendiri. Yang dimana ini adalah kebutuhan pokok masyarakat Indonesia. Produk berkualitas tinggi, stabil, dan pastinya harga sangat kompetitif karena Japang membeli langsung dari petani.
							Tidak perlu bingung mau menjual produk apa dan merk yang mana, karena JaPang akan mengirimkan sesuai dengan permintaan para JAWARA langsung ke tempat usaha masing – masing.
							Permodalannya dalam bentuk stok produk Japang, yang dimana pembayaran dilakukan sesuai jumlah barang yang laku saja.
							Pendampingan oleh tim JaPang dan diberikan beberapa pelatihan, seperti pelatihan cara menjual sampai bagaimana menjadi pengusaha yang sukses. Jadi tidak dibiarkan berusaha sendirian.
						</p>

						<p class="lh-1">    
							<b>Syarat menjadi JAWARA sangat mudah, hanya dengan kemauan keras untuk usaha, dan kemauan untuk menjadi sukses. Dengan tambahan satu lagi adalah bahwa catatan performance kredit masing – masing cukup bagus ya di bank, atau tidak ada pinjaman yang menunggak.</b>
						</p>
					</p>
				</div>
			</div>
		</div>
		<div class="container my-1">
			<div id="carouselInterval2" class="carousel slide rounded" data-bs-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active" data-bs-interval="3000">
						<img src="https://jaringpangan.com/jawara/konten/langkah.png" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item" data-bs-interval="2000">
						<img src="https://jaringpangan.com/jawara/konten/langkah1.png" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
						<img src="https://jaringpangan.com/jawara/konten/langkah2.png" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
						<img src="https://jaringpangan.com/jawara/konten/langkah3.png" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
						<img src="https://jaringpangan.com/jawara/konten/langkah4.png" class="d-block w-100" alt="...">
					</div>
					<div class="carousel-item">
						<img src="https://jaringpangan.com/jawara/konten/langkah5.png" class="d-block w-100" alt="...">
					</div>
				</div>
				<button class="carousel-control-prev" type="button" data-bs-target="#carouselInterval2" data-bs-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Previous</span>
				</button>
				<button class="carousel-control-next" type="button" data-bs-target="#carouselInterval2" data-bs-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Next</span>
				</button>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md" >
                	<img src="https://jaringpangan.com/jawara/konten/validasi.png" class="img-fluid" width="100%" alt="...">
				</div>
                <div class="col-md bg-white my-5">
                    <h1 class=" lh-1">
					<b>Tahapan Selanjutnya setelah submit data-data</b></h1>

					<p class="lh-2 my-5">
					Akan dilakukan survey oleh tim Japang baik itu secara telephone atau datang ke tempat usaha para calon JAWARA.Calon JAWARA juga akan dicek secara kesehatan keuangan (atau sering dikenal dengan Bi-checking), apabila dalam penilaian itu rapor nya merah / tidak sehat (diatas kol 1) maka Calon JAWARA lebih sulit mendapatkan persetujuan untuk menjadi JAWARA dan membutuhkan waktu lebih lama lagi.Setelah dinyatakan lolos untuk Bi-checking, maka ada beberapa dokumen yang harus ditandatangani oleh Calon JAWARA yang akan ditemui di masing - masing lokasi usaha oleh team JaPang.
					</p>

					<p class="lh-2 my-5">
						<b>Dengan menandatangani dokumen tersebut</b>, para JAWARA menyetujui semua hal yang tertera di dalam dokumen, sehingga proses selanjutnya adalah JAWARA menunggu untuk dihubungi oleh pihak Japang untuk pengiriman produk.
					</p>
                </div> 
			</div>
		</div>
                
		<div class="container-fluid" style="background-color: rgb(77, 125, 182);">
			<h2 class="lh-1 py-5 text-center text-white">
				<b>Dibawah ini adalah beberapa<br> ketentuan yang harus diketahui<br>oleh para JAWARA</b>
			</h2>
			<div class="container-sm center">
				<h6 class="lh-2 ps-5 py-5" style="color: rgb(71, 71, 71);">
					<ul class="list text-white">
						<li class="list-item">Tidak melakukan pembayaran hasil penjualan</li>
						<li class="list-item">Stok inventory barang tidak cocok dengan hasil penjualan dan inventory kunjungan sebelumnya</li>
						<li class="list-item">Tidak mendisplay beras / telur / freezer yang diberikan Japang di isi barang lain selain ayam dari Japang</li>
						<li class="list-item">Outlet Jawara tidak ditemukan</li>
						<li class="list-item">Jawara tidak bisa ditemui / tidak ada orang saat kunjungan tim Commercial Support</li>
						<li class="list-item"> Banner Jawara tidak terpasang / hilang / dalam kondisi yang tidak baik </li>
						<li class="list-item">Melakukan pembayaran dengan uang tunai, dan tidak dengan transfer ke rekening JaPang </li>
						<li class="list-item"> JAWARA menjual produk Japang ke end user secara tunai, tidak diperbolehkan menjual dengan memberikan hutang ke end user</li>
						<li class="list-item">JAWARA tidak diperbolehkan untuk menjual barang yang kualitasnya sudah menurun, dan atau menjual barang dengan kemasan Japang namun isinya berbeda. </li>
						<li class="list-item">JAWARA tidak diperbolehkan untuk menjual barang Japang dibawah harga pasar / cenderung rugi</li>
						<li class="list-item">JAWARA tidak diperbolehkan untuk saling memberikan feedback negatif ke JAWARA lain</li>
						<li class="list-item">Dalam segala kesempatan JAWARA tidak melakukan promosi secara negatif </li>
					</ul>
				</h6>
			</div>
		</div>
	</section>

	<section class="page-section m-5" id="product">
		<div class="container">
			<div class="text-center">
				<p class="section-subheading text-muted judul">Our Product</p>
            </div>
			<div class="row text-center">
				<div class="col-md-3">
					<span class="fa-stack fa-4x">
						<img class="img-fluid" src="<?php echo base_url('assets/img/product/beras.png'); ?>" width="50%">
					</span>
					<h5 class="monst">RICE</h5>
				</div>
				<div class="col-md-3">
					<span class="fa-stack fa-4x">
						<img class="img-fluid" src="<?php echo base_url('assets/img/product/telur.png'); ?>" width="50%">
					</span>
					<h5 class="monst">EGG</h5>
				</div>
				<div class="col-md-3">
					<span class="fa-stack fa-4x">
						<img class="img-fluid" src="<?php echo base_url('assets/img/product/daging.png'); ?>" width="50%">
					</span>
					<h5 class="monst">MEAT</h5>
				</div>
				<div class="col-md-3">
					<span class="fa-stack fa-4x">
						<img class="img-fluid" src="<?php echo base_url('assets/img/product/minyak.png'); ?>" width="50%">
					</span>
					<h5 class="monst">GROCERIES</h5>
				</div>
			</div>
		</div>
	</section>

	<!-- Modal -->
	<div class="modal fade" id="termsconditions" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Terms dan Conditions</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">dantimes;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>
						<b>Bagi para calon jawara:</b>
					</p>
					<p>- Proses ini tidak dikenakan biaya tambahan apapun.</p>
					<p>- Bagi para jawara yang sudah lolos seleksi dan pantas untuk mendapatkan pendanaan, jika tidak melanjutkan proses (tidak dapat dihubungi, pengunduran diri, dan lainnya) akan berdampak pada sistem blacklist untuk seluruh anggota keluarga dalam kartu keluarga sehingga tidak dapat mengajukan di kedepan harinya.</p>
					<p>- Pengunduran diri setelah penandatanganan akan dikenakan biaya penalty 5% dari total nilai pengajuan dana.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script>
        $(document).ready(function() {

            //request data kabupaten
            $('#provinsi').change(function() {
                var provinsi_id = $('#provinsi').val(); //ambil value id dari provinsi

                if (provinsi_id != '') {
                    $.ajax({
                        url: '<?= base_url(); ?>index.php/register/jawara/get_kabupaten',
                        method: 'POST',
                        data: {
                            provinsi_id: provinsi_id
                        },
                        success: function(data) {
                            $('#kab_kota').html(data)
                        }
                    });
                }
            });

			//request data kecamatan
			$('#kab_kota').change(function() {
				var kabupaten_id = $('#kab_kota').val(); // ambil value id dari kabupaten
				if (kabupaten_id != '') {
					$.ajax({
						url: '<?= base_url(); ?>index.php/register/jawara/get_kecamatan',
						method: 'POST',
						data: {
							kabupaten_id: kabupaten_id
						},
						success: function(data) {
							$('#kecamatan').html(data)
						}
					});
				}
			});
        });

		$('#kode_referral').on('change',function() {
			if(this.value=='LAINNYA'){
				$('.initial').show();
			}else{
				$(".initial").hide();
			}
		});

		$(".initial").hide();
    </script>
