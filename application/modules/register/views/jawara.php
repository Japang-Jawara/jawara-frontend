<!-- begin #content -->
        <div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb float-xl-right">
				<li class="breadcrumb-item"><a href="javascript:;">Form Registrasi</a></li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">JApang WArung RAkyat <small>(JAWARA)</small></h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row row-space-10">
				<!-- begin col-6 -->
				<div class="col-sm-12">
					<!-- begin widget-card -->
					<a href="#" class="widget-card widget-card-rounded m-b-20" data-id="widget">
						<div class="widget-card-cover" style="background-image: url(<?php echo base_url('assets/img/gallery/gallery-portrait-11-thumb.jpg'); ?>)"></div>
						<div class="widget-card-content">
							<b class="text-white">Selamat ! Selangkah lebih dekat lagi untuk memiliki bisnis dengan dimodalin oleh Jaring Pangan Indonesia.</b>
						</div>
						<div class="widget-card-content bottom">
							<i class="fab fa-pushed fa-5x text-indigo"></i>
							<h4 class="text-white m-t-10"><b>JAPANG</b></h4>
							<h5 class="f-s-12 text-white-transparent-7 m-b-2"><b>Reliable Partner for National food Chain</b></h5>
						</div>
					</a>
					<!-- end widget-card -->
				</div>
				<!-- end col-6 -->
			</div>
			<!-- end row -->
			<!-- begin wizard-form -->
			<form action="index.php/register/jawara/leds" method="POST" name="form-wizard" class="form-control-with-bg" enctype="multipart/form-data">
				<!-- begin wizard -->
				<div id="wizard">
					<!-- begin wizard-step -->
					<ul>
						<li>
							<a href="#step-1">
								<span class="number">1</span> 
								<span class="info">
									Data Diri
									<small></small>
								</span>
							</a>
						</li>
						<li>
							<a href="#step-2">
								<span class="number">2</span> 
								<span class="info">
									Upload Dokumen
									<small>Upload Foto KTP dan Upload Foto KK</small>
								</span>
							</a>
						</li>
						<li>
							<a href="#step-3">
								<span class="number">3</span>
								<span class="info">
									Lokasi Usaha
									<small></small>
								</span>
							</a>
						</li>
						<li>
							<a href="#step-4">
								<span class="number">4</span> 
								<span class="info">
									Simpan
									<small>Simpan Registrasi</small>
								</span>
							</a>
						</li>
					</ul>
					<!-- end wizard-step -->
					<!-- begin wizard-content -->
					<div>
						<!-- begin step-1 -->
						<div id="step-1">
							<!-- begin fieldset -->
							<fieldset>
								<!-- begin row -->
								<div class="row">
									<!-- begin col-8 -->
									<div class="col-xl-8 offset-xl-2">
										<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Personal info JAWARA</legend>
										
                                        <!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Email <span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
												<input type="email" name="email" placeholder="Email" data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
											</div>
										</div>
										<!-- end form-group -->

                                        <!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Nama<span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
												<input type="text" name="name" placeholder="Nama Sesuai KTP" data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
											</div>
										</div>
										<!-- end form-group -->

                                        <!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Nomor Handphone <span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
												<input type="tel" pattern="^628[0-9]{8,}$" name="phone" placeholder="628xxxxxxxxx" data-parsley-group="step-1" data-parsley-required="true" data-parsley-type="number" class="form-control" />
                                                <span style="font-size: 12px;">Note: 628xxxxxxxxx (Terdiri dari panjang minimal 10 dan selalu dimulai dengan 628)</span>
											</div>
										</div>
										<!-- end form-group -->

                                        <!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">No Induk KTP <span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
												<input type="text" pattern="^[0-9]{16,}$" name="nik_ktp" placeholder="No Induk KTP" data-parsley-group="step-1" data-parsley-required="true" data-parsley-type="number" class="form-control" />
											</div>
										</div>
										<!-- end form-group -->

                                        <!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">No Induk KK <span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
												<input type="text" pattern="^[0-9]{16,}$" name="nik_kk" placeholder="No Induk KK" data-parsley-group="step-1" data-parsley-required="true" data-parsley-type="number" class="form-control" />
											</div>
										</div>
										<!-- end form-group -->

                                        <!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Alamat Rumah<span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
                                                <textarea name="alamat_rumah" placeholder="Alamat Lengkap Rumah" data-parsley-group="step-1" data-parsley-required="true" class="form-control" rows="4" cols="50"></textarea>
                                            </div>
										</div>
										<!-- end form-group -->
									</div>
									<!-- end col-8 -->
								</div>
								<!-- end row -->
							</fieldset>
							<!-- end fieldset -->
						</div>
						<!-- end step-1 -->

						<!-- begin step-2 -->
						<div id="step-2">
							<!-- begin fieldset -->
							<fieldset>
								<!-- begin row -->
								<div class="row">
									<!-- begin col-8 -->
									<div class="col-xl-8 offset-xl-2">
										<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Upload Dokumen Anda</legend>
										<!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Upload Foto KTP Anda <span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
                                                <input type="file" name="file_ktp" accept="image/png, image/jpg, image/jpeg" ata-parsley-group="step-2" data-parsley-required="true" class="form-control"/>
                                                <span style="font-size: 12px;">Note : Ukuran Foto KTP max. 2000kb.</span>
                                            </div>
										</div>
										<!-- end form-group -->
                                        <!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Upload Foto KK Anda <span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
                                                <input type="file" name="file_kk" accept="image/png, image/jpg, image/jpeg" ata-parsley-group="step-2" data-parsley-required="true" class="form-control"/>
                                                <span style="font-size: 12px;">Note : Ukuran Foto KK max. 2000kb.</span>
                                            </div>
										</div>
										<!-- end form-group -->
									</div>
									<!-- end col-8 -->
								</div>
								<!-- end row -->
							</fieldset>
							<!-- end fieldset -->
						</div>
						<!-- end step-2 -->

						<!-- begin step-3 -->
						<div id="step-3">
							<!-- begin fieldset -->
							<fieldset>
								<!-- begin row -->
								<div class="row">
									<!-- begin col-8 -->
									<div class="col-xl-8 offset-xl-2">
										<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Lokasi Usaha Anda</legend>
										<!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Alamat Usaha<span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
                                                <textarea name="alamat_usaha" placeholder="Alamat Lengkap Rumah" data-parsley-group="step-3" data-parsley-required="true" class="form-control" rows="4" cols="50"></textarea>
                                            </div>
										</div>
										<!-- end form-group -->	
                                        <!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Provinsi <span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
												<select name="provinsi" id="provinsi" class="form-control" data-parsley-group="step-3" data-parsley-required="true">
                                                    <option value="">Pilih Provinsi</option>
                                                    <?php
                                                        foreach ($provinsi as $row) {
                                                            echo '<option value="' . $row->id . '">' . $row->name . '</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
										</div>
										<!-- end form-group -->
                                        <!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Kabupaten/Kota <span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
												<select name="kab_kota" id="kab_kota" class="form-control" data-parsley-group="step-3" data-parsley-required="true">
                                                    <option value="">Pilih Kabupaten</option>
                                                    <?php
                                                        
                                                    ?>
                                                </select>
                                            </div>
										</div>
										<!-- end form-group -->
										<!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Kodepos <span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
												<input type="text" pattern="^[0-9]{5,}$" name="kodepos" placeholder="Kodepos" data-parsley-group="step-3" data-parsley-required="true" data-parsley-type="number" class="form-control" />
											</div>
										</div>
										<!-- end form-group -->
										<!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Upload tampak depan untuk tempat yang akan digunakan sebagai JAWARA <span class="text-danger">*</span></label>
											<div class="col-lg-9 col-xl-6">
                                                <input type="file" name="file_store" accept="image/png, image/jpg, image/jpeg" ata-parsley-group="step-3" data-parsley-required="true" class="form-control"/>
                                                <span style="font-size: 12px;">Note : Ukuran Foto max. 2000kb.</span>
                                            </div>
										</div>
										<!-- end form-group -->
										<!-- begin form-group -->
										<div class="form-group row m-b-10">
											<label class="col-lg-3 text-lg-right col-form-label">Kode Referral <span class="text-danger">(JIKA ADA)</span></label>
											<div class="col-lg-9 col-xl-6">
												<input type="text" name="kode_referral" placeholder="Kode Referral" data-parsley-group="step-3" class="form-control" />
											</div>
										</div>
										<!-- end form-group -->
									</div>
									<!-- end col-8 -->
								</div>
								<!-- end row -->
							</fieldset>
							<!-- end fieldset -->
						</div>
						<!-- end step-3 -->

						<!-- begin step-4 -->
						<div id="step-4">
							<div class="jumbotron m-b-0 text-center">
								<h2 class="display-4">Apakah Anda Yakin Dengan Data Anda ?</h2>
								<p class="lead mb-4">Jika sudah Yakin, Klik button Simpan JAWARA di bawah ini.</p>
								<p><button type="submit" class="btn btn-primary btn-lg">Simpan JAWARA</button></p>
							</div>
						</div>
						<!-- end step-4 -->
					</div>
					<!-- end wizard-content -->
				</div>
				<!-- end wizard -->
			</form>
			<!-- end wizard-form -->
		</div>
		<!-- end #content -->
        
        <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
        <script>
            $(document).ready(function() {

                //request data kabupaten
                $('#provinsi').change(function() {
                    var provinsi_id = $('#provinsi').val(); //ambil value id dari provinsi

                    if (provinsi_id != '') {
                        $.ajax({
                            url: '<?= base_url(); ?>index.php/register/jawara/get_kabupaten',
                            method: 'POST',
                            data: {
                                provinsi_id: provinsi_id
                            },
                            success: function(data) {
                                $('#kab_kota').html(data)
                            }
                        });
                    }
                });

                

            });
        </script>