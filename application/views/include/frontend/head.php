<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Your description">
    <meta name="author" content="Niagahoster">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on Facebook, Twitter, LinkedIn -->
	<meta property="og:site_name" content="" /> <!-- website name -->
	<meta property="og:site" content="" /> <!-- website link -->
	<meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
	<meta name="twitter:card" content="summary_large_image"> <!-- to have large image post format in Twitter -->
	<!-- <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"> -->
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

    <!-- Webpage Title -->
    <title>DAFTAR JAWARA</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

	<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap" rel="stylesheet">
    
    <!-- Styles -->
    <!-- <link href="<?php echo base_url('assets/frontend/custom/css/bootstrap.css'); ?>" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/frontend/custom/css/swiper.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/frontend/custom/css/styles.css'); ?>" rel="stylesheet">

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->

	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="<?php echo base_url('assets/plugins/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet');?>" />
	
	<!-- v1 -->
	<link href="<?php echo base_url('assets/plugins/jvectormap-next/jquery-jvectormap.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/gritter/css/jquery.gritter.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/select2/dist/css/select2.min.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" />

	<link href="<?php echo base_url('assets/plugins/smartwizard/dist/css/smart_wizard.css');?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/plugins/nvd3/build/nv.d3.css');?>" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->
	
	<!-- Favicon  -->
    <link rel="icon" href="<?php echo base_url('assets/img/logo/japang.png'); ?>">
</head>
<body data-spy="scroll" data-target=".fixed-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-light">
        <div class="container">
            <!-- Image Logo -->
            <a class="navbar-brand" href="https://jaringpangan.com/">
                <img src="https://jaringpangan.com/jawara/konten/logojapang.png" alt="" width="130" height="40" class="d-inline-block align-text-top">
            </a>
            
            <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <!-- <span> -->
                <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#daftar">Daftar Jawara</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#jawara">Tentang Jawara</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#product">Product</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="https://jaringpangan.com" target="_blank">Japang</a>
                        </li>
                        <li class="nav-item">
                        <a class="btn btn-sm" style="background-color: rgb(226, 38, 5); color: rgb(241, 245, 10);" href="#daftar" role="button"><b>Ayo Daftar Jawara</b></a>
                        </li>
                    </ul>
                </div> <!-- end of navbar-collapse -->
            <!-- </span> -->
        </div> <!-- end of container -->
    </nav> <!-- end of navbar -->
    <div class="container-fluid" style="background-image:url(https://jaringpangan.com/jawara/konten/jawara-kilatbg.jpg)">
        <div class="container">
            <br><br><br>
            <div class="row">
                <div class="col-sm">
                    <div class="image-container ">
                        <img src="https://jaringpangan.com/jawara/konten/jawara-kilat.png" class="img-fluid" alt="logo jawara" width="90%">     
                        <center><a class="btn btn-lg m-5" style="background-color: rgb(226, 38, 5); color: rgb(241, 245, 10);" href="#daftar" role="button"><b>DAFTAR SEKARANG!</b></a></center>
                        <br>
                    </div>
                </div>
                <div class="col-md my-5" >
                    <div class="card position top-50 start-50 translate-middle rounded shadow" style="max-width: 35rem; ">
                        <div id="carouselExampleInterval" class="carousel slide rounded" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active" data-bs-interval="3000">
                                    <img src="https://jaringpangan.com/jawara/konten/Jawara-Jatim-2.jpg" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item" data-bs-interval="2000">
                                    <img src="https://jaringpangan.com/jawara/konten/Jawara-Jatim-3.jpg" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="https://jaringpangan.com/jawara/konten/Jawara-Jatim-4.jpg" class="d-block w-100" alt="...">
                                </div>
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of navigation -->